using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

public class GameObjectController : MonoBehaviour
{
    [SerializeField]
    private Button instantiateGameObjectButton;
    
    [SerializeField]
    private Button destroyLastGameObjectButton;
    
    [SerializeField]
    private Button destroyAllGameObjectsButton;

    
    // list to handle spawning and deleting certain objects
    private readonly List<GameObject> gameObjects = new List<GameObject>();

    private void Awake()
    {
        instantiateGameObjectButton.onClick.AddListener(InstantiateGameObject);
        destroyLastGameObjectButton.onClick.AddListener(DestroyLastGameObject);
        destroyAllGameObjectsButton.onClick.AddListener(DestroyAllGameObjects);
    }

    private void InstantiateGameObject()
    {
        // add created gameobject to the list & spawn the GameObject with the choosen key from the addressables
        var position = gameObjects.Count * 1.50f * Vector3.right;
        Addressables.InstantiateAsync("CubeObject", position, Quaternion.identity).Completed += handle =>
        {
            gameObjects.Add(handle.Result);
        };
    }
    
    private void DestroyLastGameObject()
    {
        // to check if we even have something to detroy
        if (gameObjects.Count <= 0)
            return;

        // to destroy the gameobject we use the "ReleaseInstance" method and we also remove it from the list
        var lastGameObject = gameObjects.Last();
        Addressables.ReleaseInstance(lastGameObject);
        gameObjects.Remove(lastGameObject);
    }
    
    private void DestroyAllGameObjects()
    {
        // go through list and remove every gameobject from the list
        foreach (var cube in gameObjects)
         Addressables.ReleaseInstance(cube); 
        
        gameObjects.Clear();

    }
}
