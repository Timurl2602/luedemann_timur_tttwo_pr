using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class TextTranslator : MonoBehaviour
{
    [SerializeField] 
    private Button translateEnglishButton;
    
    [SerializeField] 
    private Button translateGermanButton;
    
    [SerializeField] 
    private Button clearTextButton;

    [SerializeField]
    private TMP_Text textToTranslate;

    private AsyncOperationHandle textLoadingHandle;

    private void Awake()
    {
        translateEnglishButton.onClick.AddListener(TranslateEnglish);
        translateGermanButton.onClick.AddListener(TranslateGerman);
        clearTextButton.onClick.AddListener(ClearText);
    }

    private void TranslateEnglish()
    {
        LoadText("English");
    }
    private void TranslateGerman()
    {
        LoadText("German");
    }
    private void ClearText()
    {
        textToTranslate.text = string.Empty;
        Addressables.ReleaseInstance(textLoadingHandle);
    }

    private void LoadText(string languageTag)
    {
        Addressables
            .LoadAssetsAsync<TextAsset>(new List<object> {"Data", languageTag}, null, Addressables.MergeMode.Intersection)
            .Completed += OnTextLoaded;
    }

    private void OnTextLoaded(AsyncOperationHandle<IList<TextAsset>> handle)
    {
        textLoadingHandle = handle;
        textToTranslate.text = handle.Result[0].text;
    }
}
