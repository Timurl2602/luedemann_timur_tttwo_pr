using UnityEngine;
using UnityEngine.UI;


    public class QuitGame : MonoBehaviour {

        private Button button;

        private void Awake() 
        {
            button = GetComponent < Button > ();
        }

        private void OnEnable() 
        {
            button.onClick.AddListener(_ExitGame);
        }

        private void OnDisable() 
        {
            button.onClick.RemoveListener(_ExitGame);
        }

        private void _ExitGame() 
        {
            Application.Quit();
            Debug.Log("Game has been terminated");
        }
    }